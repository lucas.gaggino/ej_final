from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.bash_operator import BashOperator
from datetime import datetime, timedelta

with DAG(dag_id='monitor_ansible_dag', schedule_interval='* * 16 6 *', start_date=datetime(2020, 1, 1), catchup=False) as dag:

# Task 1
    dummy_task = DummyOperator(task_id='dummy_task')
# Task 2
    bash_task = BashOperator(task_id='run_playbook', bash_command="ansible-playbook /home/lucas/Documents/gitlab/ej_final/monitor.yml ")


dummy_task >> bash_task
